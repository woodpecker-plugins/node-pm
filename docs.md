---
name: Node PM
icon: https://codeberg.org/woodpecker-plugins/node-pm/media/branch/main/nodejs-logo-hexagon.png
author: Woodpecker Authors
description: Execute NPM, PNPM, Yarn or Bun scripts
tags: [node, npm, pnpm, yarn]
containerImage: woodpeckerci/plugin-node-pm
containerImageUrl: https://hub.docker.com/r/woodpeckerci/plugin-node-pm
url: https://codeberg.org/woodpecker-plugins/node-pm
---

# plugin-node-pm

Woodpecker plugin to install [Node.js®](https://nodejs.org/en) dependencies and execute NPM, PNPM, Yarn or Bun scripts.

## Features

- Automatically find used package manager (based on lockfile)
- Install dependencies, if `node_modules` don't exist
- Run multiple tasks

## Settings

| Settings Name         | Default                         | Description                                                                                                                                                                |
| --------------------- | ------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `run`                 | _none_                          | Script name or YAML list of multiple script names to run (see [`scripts` property in `package.json`](https://docs.npmjs.com/cli/v10/configuring-npm/package-json#scripts)) |
| `with`                | _autodetect_, fallback to `npm` | Enforce to use either `npm`, `yarn`, `pnpm` or `bun` as the package manager                                                                                                |
| `frozen_lockfile`     | `false`                         | If set to `true` (_recommended_): fails if lockfile is invalid, missing or outdated                                                                                        |
| `ignore_node_modules` | `false`                         | If set to `true` (_recommended_): run dependencies install even with existing `node_modules` directory                                                                     |

## Examples

Workflow running one script:

```yaml
pipeline:
  build:
    image: codeberg.org/woodpecker-plugins/node-pm
    settings:
      run: build # script name
      with: pnpm # if `with` is set, enforce to use this package manager
      frozen_lockfile: true # recommended
      ignore_node_modules: true # recommended
```

Workflow running multiple scripts (`prepare` and `build`):

```yaml
pipeline:
  build:
    image: codeberg.org/woodpecker-plugins/node-pm
    settings:
      run:
        - prepare
        - build
```
