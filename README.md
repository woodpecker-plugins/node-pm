# Woodpecker Node PM

[![Build status](https://ci.codeberg.org/api/badges/9651/status.svg)](https://ci.codeberg.org/repos/9651)
[![License: Apache-2.0](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Docker pulls](https://img.shields.io/docker/pulls/woodpeckerci/plugin-node-pm)](https://hub.docker.com/r/woodpeckerci/plugin-node-pm)
[![Matrix space](https://img.shields.io/matrix/woodpecker:matrix.org?label=matrix)](https://matrix.to/#/#woodpecker:matrix.org)
[![Discord chat](https://img.shields.io/discord/838698813463724034.svg?label=discord)](https://discord.gg/fcMQqSMXJy)
[![Mastodon followers](https://img.shields.io/mastodon/follow/109204843044088226?domain=https%3A%2F%2Ffloss.social&style=flat&label=mastodon)](https://floss.social/@WoodpeckerCI)

Woodpecker plugin to install [Node.js®](https://nodejs.org/en) dependencies and execute NPM, PNPM, Yarn or Bun scripts.

For the usage information and a listing of the available options please take a look at [the docs](https://woodpecker-ci.org/plugins/Node%20PM) (also available in [`docs.md` in this repository](docs.md)).

## Docker

Build the Docker image with the following command:

```console
docker build -t node-pm:dev .
```

This will build the image and load it into docker so the image can be used locally.
[More information on the output formats can be found in docker build doc](https://docs.docker.com/engine/reference/commandline/build).

## Usage

Install dependencies and build with **pnpm**:

```console
docker run --rm \
  -v $(pwd):/app -w /app \
  -e PLUGIN_RUN=build \
  -e PLUGIN_WITH=pnpm \
  node-pm:dev
```

\*Note: Replace `$(pwd):/app -w /app` with `/$(pwd):/app -w //app` for **GitBash** or with `${PWD}:/app -w /app` for **PowerShell\***

## License

This project is licensed under the Apache-2.0 License - see the [LICENSE](https://codeberg.org/woodpecker-plugins/node-pm/src/branch/main/LICENSE.txt) file for details.
