# Changelog

## [1.3.3](https://codeberg.org/woodpecker-plugins/node-pm/releases/tag/1.3.3) - 2025-01-22

### ❤️ Thanks to all contributors! ❤️

@6543, @woodpecker-bot

### 📦️ Dependency

- chore(deps): update dependency oven-sh/bun to v1.1.45 [[#51](https://codeberg.org/woodpecker-plugins/node-pm/pulls/51)]
- chore(deps): update dependency pnpm/pnpm to v10 [[#50](https://codeberg.org/woodpecker-plugins/node-pm/pulls/50)]
- chore(deps): update woodpeckerci/plugin-docker-buildx docker tag to v5.1.0 [[#48](https://codeberg.org/woodpecker-plugins/node-pm/pulls/48)]
- chore(deps): update docker.io/woodpeckerci/plugin-ready-release-go docker tag to v3 [[#47](https://codeberg.org/woodpecker-plugins/node-pm/pulls/47)]
- chore(deps): update pipelinecomponents/yamllint docker tag to v0.33.0 [[#46](https://codeberg.org/woodpecker-plugins/node-pm/pulls/46)]
- chore(deps): update dependency pnpm/pnpm to v9.14.4 [[#45](https://codeberg.org/woodpecker-plugins/node-pm/pulls/45)]

### Misc

- chore(deps): update docker.io/woodpeckerci/plugin-ready-release-go docker tag to v3.1.3 ([cdf4ca4](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/cdf4ca4a3f4e7ffa760e102e1c396e02bd23a922))
- chore(deps): update davidanson/markdownlint-cli2 docker tag to v0.17.2 ([b72b584](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/b72b5847de316dd082c3d6d08c3cd19240592dbd))
- chore(deps): update dependency oven-sh/bun to v1.1.43 ([335d91a](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/335d91a8e88a026097394afb74258d37eddc43f4))
- chore(deps): update davidanson/markdownlint-cli2 docker tag to v0.17.1 ([cb9c2c4](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/cb9c2c439342ab51ea8484091284b294fcd0755d))
- chore(deps): update davidanson/markdownlint-cli2 docker tag to v0.17.0 ([c466edc](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/c466edcd04ef9529a27b6f17da8debd4b65a9dea))
- chore(deps): update docker.io/woodpeckerci/plugin-ready-release-go docker tag to v3.1.1 ([6c693da](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/6c693dab8b2be1c5b08d85a7ed0ef5e6425ce501))
- chore(deps): update dependency pnpm/pnpm to v9.15.2 ([79e35d2](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/79e35d27a86ba5bd5656d497939762c26d25b9f9))
- chore(deps): update dependency oven-sh/bun to v1.1.42 ([7bca65c](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/7bca65c8922297d3ce7103707071f06618084bcc))
- chore(deps): update dependency pnpm/pnpm to v9.15.1 ([0e33e8d](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/0e33e8dd4987c0c37a7717e221a2f76d2fa5fe26))
- chore(deps): update davidanson/markdownlint-cli2 docker tag to v0.16.0 ([550f2eb](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/550f2ebf7bf558eab55d706dad9ba6a13b1edbce))
- chore(deps): update dependency pnpm/pnpm to v9.15.0 ([cf8e2b6](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/cf8e2b6c6364c269a51eb2a2c08bd87d02f6333c))
- Update .woodpecker/release-helper.yaml ([4c0dee7](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/4c0dee7de7e39a9e1b41d8d8cdb648fda2ab5833))
- Update CHANGELOG.md ([1bc016e](https://codeberg.org/woodpecker-plugins/node-pm/src/commit/1bc016eaec32afe8fa972634a7a13705d124c0ff))
