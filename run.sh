#!/bin/sh

set -o xtrace

pm=$PLUGIN_WITH
frozen=$PLUGIN_FROZEN_LOCKFILE
ignore_node_modules=$PLUGIN_IGNORE_NODE_MODULES

if [ -z "$pm" ]; then
  # find by lock files
  if [ -f "package-lock.json" ]; then
    pm="npm"
  elif [ -f "yarn.lock" ]; then
    pm="yarn"
  elif [ -f "pnpm-lock.yaml" ]; then
    pm="pnpm"
  elif [ -f "bun.lockb" ] || [ -f "bun.lock" ]; then
    pm="bun"
  else
    # fall back to NPM
    pm="npm"
  fi
fi

if [ ! -d "node_modules" ] || [ "$ignore_node_modules" = true ]; then
  # install first
  if [ "$frozen" = true ]; then
    # use frozen lockfile option
    if [ "$pm" = "pnpm" ]; then
      pnpm install --frozen-lockfile
    elif [ "$pm" = "yarn" ]; then
      yarn install --frozen-lockfile
    elif [ "$pm" = "bun" ]; then
      bun install --frozen-lockfile
    else
      npm ci
    fi
  else
    # install without frozen lockfile
    $pm install
  fi
fi

if ! echo "$PLUGIN_RUN" | jq ".[]" > /dev/null 2>&1; then
  # no JSON list -> only one task
  $pm run "${PLUGIN_RUN}"
else
  for i in $(echo "$PLUGIN_RUN" | jq -r ".[]"); do
    $pm run "$i"
  done
fi
