export default {
  commentOnReleasedPullRequests: false,
  // to process renovate's automerge: branch updates
  skipCommitsWithoutPullRequest: false,
};
