FROM node:22-slim

RUN --mount=type=cache,id=apt,target=/var/cache/apt \
 rm -f /etc/apt/apt.conf.d/docker-clean \
 && apt-get update \
 && apt-get install -yqq --no-install-recommends jq=1.\* curl=7.\* ca-certificates=2023\* unzip=6.\* \
 && apt-get autoremove \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

# renovate: datasource=github-releases depName=pnpm/pnpm extractVersion=^v(?<version>.*)$
ARG PNPM_VERSION=10.6.1
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"

# FIXME: https://github.com/nodejs/corepack/issues/627
RUN npm install -g corepack@0.31.0

RUN corepack enable \
 && corepack prepare pnpm@${PNPM_VERSION} --activate \
 && pnpm config set store-dir ~/.pnpm-store

# install bun
# renovate: datasource=github-releases depName=oven-sh/bun extractVersion=^bun-v(?<version>.*)$
ARG BUN_VERSION=1.2.4
ARG TARGETARCH
WORKDIR /usr/bin
RUN case "${TARGETARCH}" in \
    "amd64" ) curl -Ls "https://github.com/oven-sh/bun/releases/download/bun-v${BUN_VERSION}/bun-linux-x64.zip" -o bun.zip;; \
    "arm64" ) curl -Ls "https://github.com/oven-sh/bun/releases/download/bun-v${BUN_VERSION}/bun-linux-aarch64.zip" -o bun.zip;; \
  esac ;\
  unzip -j bun.zip && rm bun.zip

COPY run.sh /usr/local/bin/run.sh

WORKDIR /app
RUN chmod +x /usr/local/bin/run.sh
ENTRYPOINT [ "/usr/local/bin/run.sh" ]
